DROP TABLE IF EXISTS Book;

CREATE TABLE Book (
	isbn		varchar(32) PRIMARY KEY,
	title		varchar(128),
	subtitle	varchar(128),
	author		varchar(128),
	editorial	varchar(128),
	edition		int,
	year		int,
	description	text	
);

