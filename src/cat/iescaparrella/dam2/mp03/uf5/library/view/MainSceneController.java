/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iescaparrella.dam2.mp03.uf5.library.view;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.RadioMenuItem;
import javafx.stage.Stage;

/**
 *
 * @author macervero
 */
public class MainSceneController {
    
    private Stage stage;
    private ResourceBundle langResource;
    
    @FXML
    private Button catalogB;
    @FXML
    private Button addB;
    
    @FXML
    private RadioMenuItem catalanRMI;
    @FXML
    private RadioMenuItem spanishRMI;
    @FXML
    private RadioMenuItem englishRMI;
    
    public MainSceneController() {
        this.langResource = null;
        this.stage = new Stage();
    }
    
    public void showStage() throws IOException {
        this.getLanguage();
        
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getResource("fxml/MainScene.fxml");
        loader.setLocation(fxmlUrl);
        loader.setController(this);
        loader.setResources(this.langResource);
        this.stage = loader.load();
        
        /*Parent root = loader.load();
        Scene mainScene = new Scene(root);
        this.stage.setScene(mainScene);*/
        
        if(langResource.getLocale().getLanguage().toString().equals("es")) this.spanishRMI.setSelected(true);
        else if(langResource.getLocale().getLanguage().toString().equals("en")) this.englishRMI.setSelected(true);
        else this.catalanRMI.setSelected(true);
        
        this.stage.show();
    }
    
    public void onMouseClickedAddB() throws IOException {
        AddBookSceneController absController = new AddBookSceneController();
        absController.showStage();
    }
    
    public void onMouseClickedCatalogB() throws IOException {
        CatalogSceneController csController = new CatalogSceneController(this.stage);
        csController.showStage();
    }
    
    public void onActionCatalanRMI() throws FileNotFoundException, IOException {
        if(!this.catalanRMI.isSelected()) return;
        
        //1- Modificar el fitxer language.properties
        this.setLanguage("ca");
        
        //2- Recàrrega de la GUI
        this.stage.hide();
        this.showStage();
    }
    
    public void onActionSpanishRMI() throws FileNotFoundException, IOException {
        if(!this.spanishRMI.isSelected()) return;
        
        //1- Modificar el fitxer language.properties
        this.setLanguage("es");
        
        //2- Recàrrega de la GUI
        this.stage.hide();
        this.showStage();
    }
    
    public void onActionEnglishRMI() throws FileNotFoundException, IOException {
        if(!this.englishRMI.isSelected()) return;
        
        //1- Modificar el fitxer language.properties
        this.setLanguage("en");
        
        //2- Recàrrega de la GUI
        this.stage.hide();
        this.showStage();
    }
    
    private void getLanguage() throws FileNotFoundException, IOException {
        /* La classe Properties permet carregar totes les configuracions (clau-valor) d'un fitxer .properties.
         * A més a més, permet consultar-les, crear-ne de noves i modificar-les.
         * També permet crear un fitxer .properties des de 0
         */
        Properties props = new Properties();
        BufferedReader file = new BufferedReader(new FileReader("resources/config/language.properties"));
        props.load(file);
        file.close();
        
        String language = props.getProperty("language");
        
        Locale.setDefault(new Locale(language));
        this.langResource = ResourceBundle.getBundle("cat.iescaparrella.dam2.mp03.uf5.library.view.fxml.i18n.MainScene");
    }
    
    private void setLanguage(String lang) throws FileNotFoundException, IOException {
        Properties props = new Properties();
        BufferedReader fileR = new BufferedReader(new FileReader("resources/config/language.properties"));
        props.load(fileR);
        fileR.close();
        
        PrintWriter fileW = new PrintWriter("resources/config/language.properties");
        props.setProperty("language", lang);
        props.store(fileW, "");
        fileW.close();
    }
    
}
