package cat.iescaparrella.dam2.mp03.uf5.library.view;

import cat.iescaparrella.dam2.mp03.uf5.library.controller.CatalogController;
import cat.iescaparrella.dam2.mp03.uf5.library.model.Book;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.control.textfield.TextFields;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author macervero
 */
public class CatalogSceneController {
    
    private class StageOnCloseEventHandler implements EventHandler<WindowEvent> {
        @Override
        public void handle(WindowEvent t) {
            parent.show();
        }
    }
    private class StageOnOpenEventHandler implements EventHandler<WindowEvent> {
        @Override
        public void handle(WindowEvent t) {
            System.out.println("Opening CatalogScene");
        }
    }
    private class ChoiceBoxOnActionEventHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent t) {
            int idx = fieldCB.getSelectionModel().getSelectedIndex();
            ArrayList<String> items = new ArrayList<>();
                            
            if(idx == 0) {
                //Carregar els ISBN a filterACB
                for(int i=0; i<books.size(); i++) {
                    items.add((books.get(i).getIsbn()));
                }
                
            } else if(idx == 1) {
                //Carregar els Títols a filterABC
                for(int i=0; i<books.size(); i++) {
                    items.add((books.get(i).getTitle()));
                }
            } else {
                //Carregar els Autors a filterABC
                for(int i=0; i<books.size(); i++) {
                    items.add((books.get(i).getAuthor()));
                }
            }
            ObservableList<String> filterItems = FXCollections.observableArrayList(items);
            filterACB.setItems(filterItems);
            TextFields.bindAutoCompletion(filterACB.getEditor(), filterACB.getItems()); 
        }
    }
    private class ComboBoxOnActionEventHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent t) {
            int idx = fieldCB.getSelectionModel().getSelectedIndex();
            String text = filterACB.getValue();
            CatalogController cController = new CatalogController();
            ArrayList<Book> booksData = new ArrayList<>();
            
            if(idx == 0) {
                //ISBN
                booksData = cController.getBookByProperty(CatalogController.Field.ISBN, text);
            } else if(idx == 1) {
                //Title
            } else {
                //Author
            }

            ObservableList<Book> tableBooks = FXCollections.observableArrayList(booksData);
            catalogTV.setItems(tableBooks);
            ObservableList<TableColumn<Book, ?>> columns = catalogTV.getColumns();

            //setCellValueFactory(new PropertyValueFactory<>("isbn")) => agafa el getter de la propietat/atribut 'isbn' de Book
            columns.get(0).setCellValueFactory(new PropertyValueFactory<>("isbn"));
            columns.get(1).setCellValueFactory(new PropertyValueFactory<>("title"));
            columns.get(2).setCellValueFactory(new PropertyValueFactory<>("author"));
        }
    }
    
    private Stage stage;
    private Stage parent;
    private ArrayList<Book> books;
    private ResourceBundle langResource;
    
    @FXML
    private ChoiceBox<String> fieldCB;
    @FXML
    private ComboBox<String> filterACB;
    @FXML
    private TableView<Book> catalogTV;
    @FXML
    private Button editB;
    @FXML
    private Button seeB;
    
    public CatalogSceneController() {
        this.stage = new Stage();
        this.parent = null;
        this.langResource = null;
    }
    
    public CatalogSceneController(Stage parent) {
        this.stage = new Stage();
        this.parent = parent;
        this.langResource = null;
    }
    
    public void showStage() throws IOException {
        this.getLanguage();
        
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getResource("fxml/CatalogScene.fxml");
        loader.setLocation(fxmlUrl);
        loader.setController(this);
        loader.setResources(this.langResource);
        Parent root = loader.load();
        Scene mainScene = new Scene(root);
        this.stage.setScene(mainScene);
        this.stage.setOnShown(new StageOnOpenEventHandler());
        
        //Gestió de les dades de la taula
        CatalogController cController = new CatalogController();
        this.books = cController.getAllBooks();
        
        ObservableList<Book> tableBooks = FXCollections.observableArrayList(books);
        this.catalogTV.setItems(tableBooks);
        ObservableList<TableColumn<Book, ?>> columns = this.catalogTV.getColumns();
        
        //setCellValueFactory(new PropertyValueFactory<>("isbn")) => agafa el getter de la propietat/atribut 'isbn' de Book
        columns.get(0).setCellValueFactory(new PropertyValueFactory<>("isbn"));
        columns.get(1).setCellValueFactory(new PropertyValueFactory<>("title"));
        columns.get(2).setCellValueFactory(new PropertyValueFactory<>("author"));
        
        this.catalogTV.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        //Gestió del ChoiceBox
        ObservableList<String> options = FXCollections.observableArrayList(this.langResource.getString("cb.isbn"),
                                                                            this.langResource.getString("cb.title"),
                                                                            this.langResource.getString("cb.author"));
        this.fieldCB.setItems(options);
        this.fieldCB.setValue(this.langResource.getString("cb.isbn"));
        this.fieldCB.setOnAction(new ChoiceBoxOnActionEventHandler());
        
        //Gestió del ComboBox
        ArrayList<String> items = new ArrayList<>();
        for(int i=0; i<this.books.size(); i++) {
            items.add((this.books.get(i).getIsbn()));
        }
        ObservableList<String> filterItems = FXCollections.observableArrayList(items);
        this.filterACB.setItems(filterItems);
        this.filterACB.setEditable(true);
        TextFields.bindAutoCompletion(this.filterACB.getEditor(), this.filterACB.getItems()); 
        this.filterACB.setOnAction(new ComboBoxOnActionEventHandler());
        
        //Modificació els botons per carregar les icones
        this.editB.setText("");
        this.editB.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.EDIT).size(24));
        this.seeB.setText("");
        this.seeB.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.EYE).size(24));
        
        //Gestió de la pantalla pare
        if(this.parent != null) {
            //Configurarem el handler per reobrir la MainScene quan cantanquen la CatalogScene
            this.stage.setOnCloseRequest(new StageOnCloseEventHandler());
            this.stage.setOnHidden(new StageOnCloseEventHandler());
            
            //Configuració dels events mitjançant una funció lambda
            /*this.stage.setOnCloseRequest((WindowEvent w) -> {
                this.parent.show();
                this.func();
            });
            this.stage.setOnHidden((WindowEvent w) -> {
                this.parent.show();
                this.func();
            });*/
            
            //Amagarem la MainScene
            this.parent.hide();
        }
        this.stage.show();
    }
    
    public void onMouseClickedCatalogTV() {
        ObservableList<Book> selectedItems = this.catalogTV.getSelectionModel().getSelectedItems();
        for(int i=0; i<selectedItems.size(); i++) {
            System.out.println(selectedItems.get(i).getIsbn());
        }
    }
    
    private void getLanguage() throws FileNotFoundException, IOException {
        /* La classe Properties permet carregar totes les configuracions (clau-valor) d'un fitxer .properties.
         * A més a més, permet consultar-les, crear-ne de noves i modificar-les.
         * També permet crear un fitxer .properties des de 0
         */
        Properties props = new Properties();
        BufferedReader file = new BufferedReader(new FileReader("resources/config/language.properties"));
        props.load(file);
        
        String language = props.getProperty("language");
        
        Locale.setDefault(new Locale(language));
        this.langResource = ResourceBundle.getBundle("cat.iescaparrella.dam2.mp03.uf5.library.view.fxml.i18n.CatalogScene");
    }
}
