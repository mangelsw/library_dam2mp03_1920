/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iescaparrella.dam2.mp03.uf5.library.view;

import cat.iescaparrella.dam2.mp03.uf5.library.controller.CatalogController;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 *
 * @author macervero
 */
public class AddBookSceneController {
    
    private Stage stage;
    
    @FXML
    private TextField titleTF;
    @FXML
    private TextField subtitleTF;
    @FXML
    private TextField authorTF;
    @FXML
    private TextField isbnTF;
    @FXML
    private TextField editorialTF;
    @FXML
    private TextField editionTF;
    @FXML
    private TextField yearTF;
    @FXML
    private TextArea descpTA;
    @FXML
    private Button addB;
    
    public AddBookSceneController() {
        this.stage = new Stage();
    }
    
    public void showStage() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getResource("fxml/AddBookScene.fxml");
        loader.setLocation(fxmlUrl);
        loader.setController(this);
        Parent root = loader.load();
        Scene mainScene = new Scene(root);
        mainScene.getStylesheets().add(getClass().getResource("fxml/css/AddBookScene.css").toExternalForm());
        this.stage.setScene(mainScene);
        
        this.stage.show();
    }
    
    public void onKeyTypedEditionTF() {
        Pattern regex = Pattern.compile("[0-9]+");
        Matcher matcher = regex.matcher(this.editionTF.getText());
        if(!matcher.matches()) {
            //Acció d'avís a l'usuari
            if(!this.editionTF.getStyleClass().contains("tfNumeric")) {
                this.editionTF.getStyleClass().add("tfNumeric");
            }
        } else {
            System.out.println(this.editionTF.getStyleClass().indexOf("tfNumeric"));
            this.editionTF.getStyleClass().remove("tfNumeric");
        }
    }
    
    public void onMouseClickedAddB() {
        CatalogController catalog = new CatalogController();
        int edition = 0;
        int year = 0;
        boolean error = false;
        
        try {
            edition = Integer.parseInt(this.editionTF.getText());
        } catch(NumberFormatException e) {
            error = true;
            Alert alertDialog = new Alert(Alert.AlertType.ERROR, "Atenció, el camp \"Edició\" ha de ser numèric.");
            alertDialog.show();
        }
        
        try {
            year = Integer.parseInt(this.yearTF.getText());
        } catch(NumberFormatException e) {
            error = true;
        }
            
        if(!error) {
            catalog.insertBook(this.isbnTF.getText(), this.titleTF.getText(),
                            this.subtitleTF.getText(), this.authorTF.getText(),
                            this.editorialTF.getText(), edition, year,
                            this.descpTA.getText());
        }
    }
    
    public void onMouseClickedLoadXMLB() {
        FileChooser fileDialog = new FileChooser();
        fileDialog.setTitle("Escull el fitxer a carregar");
        fileDialog.getExtensionFilters().add(new ExtensionFilter("Fitxer XML", "*.xml"));
        File xmlFile = fileDialog.showOpenDialog(this.stage);
        if(xmlFile != null) {
            System.out.println(xmlFile.getPath());
        }
    }
    
}
