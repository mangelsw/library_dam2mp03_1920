package cat.iescaparrella.dam2.mp03.uf5.library.controller;

import cat.iescaparrella.dam2.mp03.uf5.library.model.Book;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author macervero
 */
public class CatalogController {
    
    public enum Field {ISBN, TITLE, AUTHOR};
    
    public CatalogController() {}
    
    public boolean insertBook(String isbn, String title, String subtitle,
                                String author, String editorial, int edition,
                                int year, String description) {
        Book book = new Book();
        book.setIsbn(isbn);
        book.setTitle(title);
        book.setSubtitle(subtitle);
        book.setAuthor(author);
        book.setEditorial(editorial);
        book.setEdition(edition);
        book.setYear(year);
        book.setDescription(description);
        
        return book.insertBook();
    }
    
    public boolean insertBookFromXML(File xmlFile) {
        /*
         * Opció 1
         *  1- Llegir l'XML al Controller
         *  2- Fer els inserts sobre Book, un cop llegit l'XML
         *
         * Opció 2
         *  1- Fer que sigui el propi Book qui llegeixi l'XML i faci els insert
         */
        
        return true;
    }
    
    public ArrayList<Book> getAllBooks() {
        Book book = new Book();
        return book.getAllBooks();
    }
    
    public ArrayList<Book> getBookByProperty(Field field, String value) {
        Book book = new Book();
        if(field == Field.ISBN) return book.getBooksByIsbn(value);
        /*else if(field == Field.TITLE) return book.getBooksByTitle(value);
        else if(field == Field.AUTHOR) return book.getBooksByAythor(value);*/
        else return new ArrayList<Book>();
    }
}
