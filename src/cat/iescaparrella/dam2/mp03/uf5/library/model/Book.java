
package cat.iescaparrella.dam2.mp03.uf5.library.model;

import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author macervero
 */
public class Book {
    
    private String isbn;
    private String title;
    private String subtitle;
    private String author;
    private String editorial;
    private int edition;
    private int year;
    private String descp;
    
    private DBConnection dbconnection;
    
    public Book() {
        this.isbn = "";
        this.title = "";
        this.subtitle = "";
        this.author = "";
        this.editorial = "";
        this.edition = 0;
        this.year = 0;
        this.descp = "";
        
        this.dbconnection = new DBConnection();
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setDescription(String descp) {
        this.descp = descp;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public String getTitle() {
        return this.title;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getEditorial() {
        return this.editorial;
    }

    public int getEdition() {
        return this.edition;
    }

    public int getYear() {
        return this.year;
    }

    public String getDescp() {
        return this.descp;
    }
    
    public boolean insertBook() {
        String query = "INSERT INTO Book VALUES('" + this.isbn + "',"
                + "                             '" + this.title + "',"
                + "                             '" + this.subtitle + "',"
                + "                             '" + this.author + "',"
                + "                             '" + this.editorial + "',"
                + "                             " + this.edition + ","
                + "                             " + this.year + ","
                + "                             '" + this.descp + "');";
        this.dbconnection.connect();
        int nrows = this.dbconnection.executeInsert(query);
        this.dbconnection.disconnect();
        
        if(nrows == 1) return true;
        return false;
    }
    
    public ArrayList<Book> getAllBooks() {
        String query = "SELECT * FROM Book;";
        
        this.dbconnection.connect();
        ResultSet result = this.dbconnection.executeSelect(query);
        ArrayList<Book> books = this.getBooksFromResultSet(result);
        this.dbconnection.disconnect();
        return books;
    }
    
    public ArrayList<Book> getBooksByIsbn(String isbn) {
        String query = "SELECT * FROM Book WHERE isbn = '" + isbn + "';";
        
        this.dbconnection.connect();
        ResultSet result = this.dbconnection.executeSelect(query);
        ArrayList<Book> books = this.getBooksFromResultSet(result);
        this.dbconnection.disconnect();
        return books;
    }
    
    private ArrayList<Book> getBooksFromResultSet(ResultSet result) {
        ArrayList<Book> books = new ArrayList<>();
        try {
            Book book;
            while(result.next()) {
                book = new Book();
                book.isbn = result.getString("isbn");
                book.title = result.getString("title");
                book.subtitle = result.getString("subtitle");
                book.author = result.getString("author");
                book.editorial = result.getString("editorial");
                book.edition = result.getInt("edition");
                book.year = result.getInt("year");
                book.descp = result.getString("description");
                books.add(book);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return books;
    }
    
}
