package cat.iescaparrella.dam2.mp03.uf5.library.model;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author macervero
 */
public class DBConnection {
    
    private final String server;
    private final String dbname;
    private final String uname;
    private final String passwd;
    private final String url;
    private Connection dbconnection;
    private Statement statement;
    private ResultSet result;
    
    public DBConnection () {
        this.server = "192.168.1.219";  //Els que aneu amb màquina virtual, poseu la IP de la màquina
        this.dbname = "librarydb";
        this.uname = "postgres";
        this.passwd = "postgres";   //Si l'heu instal·lat a partir de l'instal·lador, el password és "".
                                    //Si l'heu instal·lat manualment, el password que hagueu donat a postgres
                                    
        this.url = "jdbc:postgresql://" + this.server + "/" + this.dbname + "?user=" + this.uname + "&password=" + this.passwd;
        this.dbconnection = null;
        this.statement = null;
        this.result = null;
    }
    
    public boolean connect() {
        try {
            this.dbconnection = DriverManager.getConnection(this.url);
            this.statement = this.dbconnection.createStatement();
        } catch(SQLException e) {
            //DEBUG
            e.printStackTrace();
            this.dbconnection = null;
            this.statement = null;
            return false;
        }
        return true;
    }
    
    public void disconnect() {
        try {
            if(this.result != null) this.result.close();
            if(this.statement != null) this.statement.close();
            if(this.dbconnection != null) this.dbconnection.close();
        } catch(SQLException e) {
            //DEBUG
            e.printStackTrace();
        }
    }
    
    public int executeInsert(String query) {
        int nrows;
        try {
            nrows = this.statement.executeUpdate(query);
        } catch(SQLException e) {
            //DEBUG
            e.printStackTrace();
            nrows = 0;
        }
        return nrows;
    }
    
    public ResultSet executeSelect(String query) {
        try {
            this.result = this.statement.executeQuery(query);
        } catch(SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this.result;
    }
    
}
