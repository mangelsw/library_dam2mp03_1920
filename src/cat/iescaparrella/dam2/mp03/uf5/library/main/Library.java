package cat.iescaparrella.dam2.mp03.uf5.library.main;

import cat.iescaparrella.dam2.mp03.uf5.library.view.MainSceneController;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author macervero
 */
public class Library extends Application {

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage stage) throws Exception {        
        MainSceneController mainController = new MainSceneController();
        mainController.showStage();
    }
    
}
